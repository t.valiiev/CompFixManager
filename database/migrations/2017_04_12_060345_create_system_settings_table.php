<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_settings', function (Blueprint $table) {
            $table->string('key')->unique();
            $table->string('value');

            $table->primary('key');
        });

        Schema::table('groups', function (Blueprint $table) {
            $table->boolean('can_update_system_settings')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_settings');
        Schema::table('system_settings', function (Blueprint $table) {
            $table->removeColumn('can_update_system_settings');
        });
    }
}
