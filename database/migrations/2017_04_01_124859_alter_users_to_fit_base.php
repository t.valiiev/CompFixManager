<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersToFitBase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('group_id')->unsigned()->nullable();
            $table->string('phone_number')->nullable();
            $table->string('comment')->nullable();
            $table->string('allowed_ip')->nullable();
            $table->boolean('is_active')->default(true);

            $table
                ->foreign('group_id')
                ->references('id')
                ->on('groups')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->removeColumn('group_id');
            $table->removeColumn('phone_number');
            $table->removeColumn('comment');
            $table->removeColumn('allowed_ip');
            $table->removeColumn('is_active');

            $table->dropForeign(['group_id']);
        });
    }
}
