<?php

namespace App\Console\Commands;

use App\Group;
use App\StatusOption;
use Illuminate\Console\Command;

/**
 * Class DbInitializer
 * @package App\Console\Commands
 */
class DbInitializer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:init {--seed : Seed database with demo data} {--force : Force refreshing and seeding}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populates database with initial data';


    /**
     * DbInitializer constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $force = $this->option('force')? ['--force' => true] : [];
        $this->call('migrate:refresh', $force);

        $this->info('Creating admin group...');
        $fields = ['users', 'groups', 'orders', 'clients', 'devices'];
        $admin_group_fields = [
            'name' => 'Admin',
            'can_update_system_settings' => 1,
        ];
        foreach ($fields as $field) {
            $admin_group_fields["can_view_$field"] = 1;
            $admin_group_fields["can_create_update_$field"] = 1;
            $admin_group_fields["can_delete_$field"] = 1;
        }
        $admin_group = Group::create($admin_group_fields);
        $this->info('Done.');

        $this->info('Creating admin user...');
        $admin_group->users()->create([
            'name' => 'Admin',
            'password' => 'password',
            'email' => 'admin@example.com',
            'api_token' => str_random(60),
        ]);
        $this->info('Done.');

        $this->info('Creating initial status for orders...');
        StatusOption::create(['name' => 'New']);
        $this->info('Done.');

        $needSeeding = $this->option('seed') ? 1 : 0;
        if ($needSeeding) {
            $this->call('db:seed', $force);
        }
    }
}
