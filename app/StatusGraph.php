<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class StatusGraph
 *
 * Фактически является обьектом связи между опшинами. Связывает направление изменения.
 *
 * @package App
 */
class StatusGraph extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'status_graph';

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Опшн, который можно изменить.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function from()
    {
        return $this->belongsTo(StatusOption::class, 'from_id');
    }

    /**
     * Опшн, к которому идет.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function to()
    {
        return $this->belongsTo(StatusOption::class, 'to_id');
    }
}
