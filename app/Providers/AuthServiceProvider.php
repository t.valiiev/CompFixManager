<?php

namespace App\Providers;

use App\Client;
use App\Device;
use App\Group;
use App\Order;
use App\Policies\ClientPolicy;
use App\Policies\DevicePolicy;
use App\Policies\GroupPolicy;
use App\Policies\OrderPolicy;
use App\Policies\SystemSettingsPolicy;
use App\Policies\UserPolicy;
use App\SystemSetting;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        User::class => UserPolicy::class,
        Group::class => GroupPolicy::class,
        SystemSetting::class => SystemSettingsPolicy::class,
        Order::class => OrderPolicy::class,
        Client::class => ClientPolicy::class,
        Device::class => DevicePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
