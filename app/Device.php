<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Класс Устройство.
 *
 * @package App
 * @property integer id Идентификатор устройства в системе.
 * @property integer client_id Идентификатор клиента, которому принадлежит устройство.
 * @property string type Тип устройства (планшет, пк, мобильный тел. и подобное).
 * @property string hwid Уникальный идентификатор HWID устройства.
 * @property string descriprion Описание устройства.
 */
class Device extends Model
{
    /**
     * Сколько записей на страницу в api.
     *
     * @var int
     */
    public $apiPerPage = 20;

    /**
     * Массово присваиваемые поля.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'hwid', 'description', 'client_id'
    ];

    /**
     * Клиент, которому принадлежит устройство.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * Заказы, в которых учавствует данное устройство.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function orders()
    {
        return $this->hasMany(Order::class, 'device_id');
    }

    /**
     * Убеждаемся, что описание товара как минимум пустая строка.
     *
     * @param string $val
     */
    function setDescriptionAttribute($val)
    {
        $this->attributes['description'] = $val ?? "";
    }
}
