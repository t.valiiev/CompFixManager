<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class Client
 * @package App
 */
class Client extends Authenticatable
{
    use Notifiable;

    /**
     * Количество результатов в выборке api.
     *
     * @var int
     */
    public static $apiPerPage = 20;

    /**
     * Поля, которые можно массово заполнять.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'comment', 'email', 'phone', 'password'
    ];

    /**
     * Список устройств, пренадлежащих клиенту.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function devices()
    {
        return $this->hasMany(Device::class);
    }

    /**
     * Заказы, пренадлежащие клиенту.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function orders()
    {
        return $this->hasMany(Order::class);
    }

    /**
     * @param $val
     */
    function setPasswordAttribute($val)
    {
        $this->attributes['password'] = bcrypt($val);
    }
}
