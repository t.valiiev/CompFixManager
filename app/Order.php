<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Класс Заказ.
 * @package App
 * @property integer id Идентификатор заказа.
 * @property Carbon created_at Дата создания заказа.
 * @property Carbon updated_at Дата последнего изменения любого из атрибутов заказа.
 * @property integer created_id Идентификатор пользователя, создавшего заказ.
 * @property integer client_id Идентификатор клиента, к которому относится данный заказ.
 * @property integer device_id Идентификатор устройства, к которому отностися данный заказ.
 * @property string task Задача по заказу.
 * @property string comment Комментарий к заказу.
 */
class Order extends Model
{
    /**
     * Массово присваиваемые поля.
     *
     * @var array
     */
    protected $fillable = [
        'task', 'comment', 'created_id', 'device_id', 'client_id'
    ];

    /**
     * Пользователь (сотрудник), создавший заказ.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    function creator(){
        return $this->belongsTo(User::class, 'created_id');
    }

    /**
     * Клиент, осуществивший заказ.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    function client(){
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * Устройство, которого касается заказ.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    function device(){
        return $this->belongsTo('App\Device');
    }

    /**
     * Все статусы заказа.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function statuses() {
        return $this->hasMany('App\Status', 'order_id');
    }

    /**
     * Текущий (последний) статус заказа.
     *
     * @return \App\Status
     */
    function getCurrentStatus() {
        return $this->hasMany('App\Status', 'order_id')->orderBy('created_at', 'desc')->first();
    }

    /**
     * Имя текущего (последнего) статуса заказа.
     *
     * @return \App\Status
     */
    function getCurrentStatusName()
    {
        return $this->hasMany('App\Status', 'order_id')->orderBy('created_at', 'desc')->first()->option->name ?? '---';
    }

    /**
     * @param string $val
     */
    function setCommentAttribute($val)
    {
        $this->attributes['comment'] = $val ?? "";
    }
}
