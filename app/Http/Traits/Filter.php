<?php

namespace Traits;

use Illuminate\Database\Eloquent\Builder;

/**
 * Класс Filterable
 *
 * Предназначен для добавления функционала в контроллер. Главное
 * предназначение - возожность фильтрации записей в зависимости от
 * приподнесенной конфигурации. Он главным образом предназначен для
 * вставки в контроллеры.
 *
 * @package Traits
 */
trait Filter
{
    /**
     * Данный метод позволяет фильтровать запрос.
     *
     * Ожидается следующие параметры:
     * Builder $query:
     *  Этот параметр - строитель запросов. Получить
     *  его можно так: $query = Model::query();
     *
     * $conditions:
     *  Ассоциативный массив вида
     *  [
     *      "<name_of_field1>:<action_type1>" : "<search_value1>",
     *      "<name_of_field2>:<action_type2>" : "<search_value2>",
     *      .......
     *  ]
     *
     *  Где:
     *  name_of_field - Имя искомого поля.
     *  action_type - Тип сравнения (больше, меньше, равно, подобно).
     *  search_value - Значение, по которому нужно сравнивать.
     *
     * string $type:
     *  Тип выборки (and, or)
     *
     * @param Builder $query
     * @param $conditions
     * @param string $type
     * @return Builder
     */
    private function filter(Builder $query, $conditions, $type = 'and')
    {
        if (count($conditions) > 0) {
            foreach ($conditions as $k => $v) {
                if ($v == '') continue;

                $attr_array = explode(':', $k);
                if(!isset($attr_array[1]))
                    continue;

                list($field_name, $action_type) = $attr_array;
                switch ($action_type) {
                    case 'eq':
                        if ($type == 'and') {
                            $query = $query->where($field_name, '=', "$v");
                        } elseif ($type == 'or') {
                            $query = $query->orWhere($field_name, '=', "$v");
                        }
                        break;
                    case 'like':
                        if ($type == 'and') {
                            $query = $query->where($field_name, 'like', "%$v%");
                        } elseif ($type == 'or') {
                            $query = $query->orWhere($field_name, 'like', "%$v%");
                        }
                }
            }
        }

        return $query;
    }
}