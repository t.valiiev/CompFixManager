<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlockByIp
{
    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $allowed_ip_str = Auth::user()->allowed_ip;
        if ($allowed_ip_str) {
            $valid_ip = false;
            $allowed_ips = explode(',', $allowed_ip_str);
            $client_ip = $request->ip();
            foreach ($allowed_ips as $allowed_ip) {
                if ($this->ip_in_range($client_ip, $allowed_ip)) {
                    $valid_ip = true;
                    break;
                }
            }

            if (!$valid_ip) {
                return redirect(route('welcome', ['m' => 1]));
            }
        }

        return $next($request);
    }

    /**
     * Check if a given ip is in a network
     *
     * @param  string $ip IP to check in IPV4 format eg. 127.0.0.1
     * @param  string $range IP/CIDR netmask eg. 127.0.0.0/24, also 127.0.0.1 is accepted
     * @return boolean true if the ip is in this range / false if not.
     */
    private function ip_in_range($ip, $range)
    {
        if (strpos($range, '/') == false) {
            return $ip == $range;
        }
        // $range is in IP/CIDR format eg 127.0.0.1/24
        list($range, $netmask) = explode('/', $range, 2);
        $range_decimal = ip2long($range);
        $ip_decimal = ip2long($ip);
        $wildcard_decimal = pow(2, (32 - $netmask)) - 1;
        $netmask_decimal = ~$wildcard_decimal;
        return (($ip_decimal & $netmask_decimal) == ($range_decimal & $netmask_decimal));
    }
}
