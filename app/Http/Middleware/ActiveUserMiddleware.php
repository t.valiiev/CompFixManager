<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ActiveUserMiddleware
{
    /**
     * Проверка, является ли пользователь активным.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->is_active != "1") {
            return redirect(route('welcome', ['m' => 0]));
        }
        return $next($request);
    }
}
