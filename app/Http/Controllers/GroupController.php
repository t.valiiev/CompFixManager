<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;

/**
 * Класс GroupsController
 * @package App\Http\Controllers
 */
class GroupController extends Controller
{
    /**
     * Страница просмотра всех групп.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function index(Request $request)
    {
        $this->authorize('view', Group::class);

        $groups = Group::paginate();
        $groups = $groups->appends($request->except('page'));
        return view('groups.index', compact('groups'));
    }

    /**
     * Редактирование группы.
     *
     * @param Group $group
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function showEditForm(Group $group)
    {
        $this->authorize('update', Group::class);

        return view('groups.edit', compact('group'));
    }

    /**
     * Апдейт группы в базе данных.
     *
     * @param Request $request
     * @param Group $group
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function update(Request $request, Group $group)
    {
        $this->authorize('update', Group::class);
        $data = $request->all();

        $items = ['users', 'groups', 'orders', 'clients', 'devices'];
        foreach ($items as $item) {
            $data["can_view_$item"] = $request->has("can_view_$item") ? 1 : 0;
            $data["can_create_update_$item"] = $request->has("can_create_update_$item") ? 1 : 0;
            $data["can_delete_$item"] = $request->has("can_delete_$item") ? 1 : 0;
        }

        $group->fill($data);
        $group->save();
        return redirect(route('groups.index'));
    }

    /**
     * Удаление группы.
     *
     * @param Group $group
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function delete(Group $group)
    {
        $this->authorize('delete', Group::class);

        $group->delete();

        return redirect()->back();
    }

    /**
     * Просмотр одной группы.
     *
     * @param Group $group
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function show(Group $group)
    {
        $this->authorize('view', Group::class);

        return view('groups.show', compact('group'));
    }

    function store(Request $request)
    {
        $this->authorize('create', Group::class);

        $data = $request->all();

        $items = ['users', 'groups', 'orders', 'clients', 'devices'];
        foreach ($items as $item) {
            $data["can_view_$item"] = $request->has("can_view_$item") ? 1 : 0;
            $data["can_create_update_$item"] = $request->has("can_create_update_$item") ? 1 : 0;
            $data["can_delete_$item"] = $request->has("can_delete_$item") ? 1 : 0;
        }
        
        Group::create($data);
        return redirect(route('groups.index'));
    }

    function showCreateForm()
    {
        $this->authorize('create', Group::class);

        return view('groups.create');
    }
}
