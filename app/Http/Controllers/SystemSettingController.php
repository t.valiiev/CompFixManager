<?php

namespace App\Http\Controllers;

use App\Group;
use App\Helpers\SettingsStore;
use App\Status;
use App\StatusGraph;
use App\StatusOption;
use App\SystemSetting;
use Illuminate\Http\Request;

/**
 * Класс SystemSettingController
 * @package App\Http\Controllers
 */
class SystemSettingController extends Controller
{
    /**
     * @var SettingsStore
     */
    private $settingsStore;

    /**
     * SystemSettingController constructor.
     * @param SettingsStore $settingsStore
     */
    public function __construct(SettingsStore $settingsStore)
    {
        $this->settingsStore = $settingsStore;
    }


    /**
     * Показывает форму для апдейта настроек.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function showUpdateForm()
    {
        $this->authorize('update', SystemSetting::class);

        $groups = Group::all();
        $settings = $this->settingsStore->getAll();
        $status_options = StatusOption::all();

        $status_graph_t = StatusGraph::all();
        $status_graph = [];
        foreach ($status_graph_t as $status) {
            $status_graph["{$status->from->id}_{$status->to->id}"] = true;
        }

        return view('system_settings', compact('groups', 'settings', 'status_options', 'status_graph'));
    }

    /**
     * Апдейтит настройки.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function updateBasicSettings(Request $request)
    {
        $this->authorize('update', SystemSetting::class);

        $data = $request->all();
        unset($data['_token']);

        $this->settingsStore->setBulk($data);

        return redirect(route('system_settings'));
    }

    /**
     * Апдейтит настройки графа перехода статусов.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function updateStatusGraph(Request $request)
    {
        $this->authorize('update', SystemSetting::class);

        StatusGraph::truncate();

        $data = $request->all();
        $this->settingsStore->setValue('default_status_option_id', $data['default_status_option_id']);

        unset($data['_token']);
        unset($data['default_status_option_id']);

        foreach ($data as $k => $v) {
            $split = explode('_', $k);
            StatusGraph::create(['from_id' => $split[0], 'to_id' => $split[1]]);
        }

        return redirect(route('system_settings'));
    }

    /**
     * Добавление статуса.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function addStatusOption(Request $request)
    {
        $this->authorize('update', SystemSetting::class);

        StatusOption::create($request->all());

        return redirect(route('system_settings'));
    }

    /**
     * Удаление статуса.
     *
     * @param Request $request
     * @return string
     */
    function deleteStatusOption(Request $request)
    {
        $this->authorize('update', SystemSetting::class);

        StatusOption::findOrFail($request->input('id', '-1'))->delete();

        return 'ok';
    }

    /**
     * Изменение имени статуса.
     *
     * @param Request $request
     * @param StatusOption $statusOption
     * @return string
     */
    function updateStatusOption(Request $request)
    {
        $this->authorize('update', SystemSetting::class);

        $statusOption = StatusOption::find($request->input('id'));
        $statusOption->fill($request->all());
        $statusOption->save();

        return 'ok';
    }
}
