<?php

namespace App\Http\Controllers;

use App\Client;
use App\Device;
use App\Favorite;
use App\Helpers\SettingsStore;
use App\Order;
use App\Status;
use App\StatusGraph;
use App\StatusOption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Traits\Filter;
use Illuminate\Support\Facades\Mail;

/**
 * Class OrderController
 * @package App\Http\Controllers
 */
class OrderController extends Controller
{
    use Filter;

    /**
     * @var SettingsStore
     */
    private $settingsStore;

    /**
     * OrderController constructor.
     * @param SettingsStore $settingsStore
     */
    function __construct(SettingsStore $settingsStore)
    {
        $this->settingsStore = $settingsStore;
    }

    /**
     * Просмотр всех заказов (с фильтрацией).
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function index(Request $request)
    {
        $this->authorize('view', Order::class);

        $query = $request->query();

        $orders = $this->filter(Order::query(), $query);

        if (isset($query['device_hwid'])) {
            $orders = $orders
                ->join('devices', 'devices.id', '=', 'orders.device_id')
                ->select('orders.*')
                ->where("devices.hwid", 'like', "%" . $query['device_hwid'] . "%");
        }

        if (isset($query['client_name'])) {
            $orders = $orders
                ->join('clients', 'clients.id', '=', 'orders.client_id')
                ->select('orders.*')
                ->where('clients.name', 'like', "%" . $query['client_name'] . "%");
        }

        if (isset($query['status_option_id'])) {
            $orders = $orders
                ->whereIn('orders.id', function($q) use ($query) {
                    $q->select('os1.order_id')->from('statuses as os1')->whereIn('os1.id', function($q) {
                        $q->select('os2.id')->from('statuses as os2')->leftJoin('statuses as os3', function($join) {
                            $join->on('os2.order_id','=','os3.order_id');
                            $join->on('os2.created_at','<','os3.created_at');
                        })->whereNull('os3.order_id');
                    })->where('os1.status_option_id', '=', $query['status_option_id']);
                })->select('orders.*');

            /*    ->join('statuses', 'statuses.order_id', '=', 'orders.id')
                ->select('orders.*')
                ->where('statuses.status_option_id', '=', $query['status_option_id']); */
        }

        $orders = $orders->paginate();
        $orders->appends($request->except('page'));

        $status_options = StatusOption::all();

        return view('orders.index', compact('orders', 'query', 'status_options'));
    }

    /**
     * Показ формы создания нового заказа.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function create(Request $request)
    {
        $this->authorize('create', Order::class);

        $clients = [];
        $devices = [];

        $d_id = $request->query('device_id');
        if ($d_id) {
            $devices[] = Device::find($d_id);
            $clients[] = $devices[0]->client;
        }

        return view('orders.create', compact('clients', 'devices', 'request'));
    }

    /**
     * Показ усовершенственной ("умной") формы создания заказа
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function create2()
    {
        $this->authorize('create', Order::class);

        return view('orders.create2');
    }

    function store2(Request $request)
    {
        $data = $request->all();

        $is_new_client = $data['_new_client'];
        $is_new_device = $data['_new_device'];

        // Делаем разделение данных, чтобы дальше не путаться
        $client_data = [];
        $device_data = [];
        $order_data = [];

        foreach ($data as $k => $v) {
            $k = preg_replace('/^client_/', '', $k, -1, $count);
            if ($count > 0) {
                $client_data[$k] = $v;
                continue;
            }

            $k = preg_replace('/^device_/', '', $k, -1, $count);
            if ($count > 0) {
                $device_data[$k] = $v;
                continue;
            }

            $k = preg_replace('/^order_/', '', $k, -1, $count);
            if ($count > 0) {
                $order_data[$k] = $v;
                continue;
            }
        }

        if ($is_new_client == '1') {
            $client_data['password'] = str_random(8);
            $client = Client::create($client_data);

            Mail::send('mails.client_account_created', ['data' => $client_data], function ($m) use ($client) {
                $m->from('hello@compfix.com', 'CompFix');
                $m->to($client->email, $client->name)->subject('Your new account');
            });

        } else {
            $client = Client::find($client_data['id']);
        }

        if ($is_new_device == '1') {
            $device_data['client_id'] = $client->id;
            $device = Device::create($device_data);
        } else {
            $device = Device::find($device_data['id']);
        }

        $current_user_id = Auth::user()->id;

        $order_data['client_id'] = $client->id;
        $order_data['device_id'] = $device->id;
        $order_data['created_id'] = $current_user_id;
        $order = Order::create($order_data);

        // Добавления статуса к заказу
        $order->statuses()->create([
            'status_option_id' => $this->settingsStore->getValue('default_status_option_id'),
            'changed_id' => $current_user_id,
            'comment' => '',
        ]);

        return redirect(route('orders.show', $order->id));
    }

    /**
     * Просмотр формы редактирование заказа.
     *
     * @param Order $order
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function edit(Order $order)
    {
        $this->authorize('update', Order::class);

        $devices = [];
        if ($order->device->client->id == $order->client->id) {
            $devices = $order->client->devices;
        } else {
            $devices = [$order->device];
        }
        $clients = [$order->client];

        $available_statuses = [$order->getCurrentStatus()->option];
        $graphs = StatusGraph::where('from_id', $order->getCurrentStatus()->option->id)->get();
        foreach ($graphs as $graph) {
            $available_statuses[] = $graph->to;
        }

        return view('orders.edit', compact('order', 'devices', 'clients', 'available_statuses'));
    }

    /**
     * Создание нового заказа.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function store(Request $request)
    {
        $this->authorize('create', Order::class);

        $current_user_id = Auth::user()->id;

        $data = array_merge($request->all(), ['created_id' => $current_user_id]);
        $order = Order::create($data);

        // Добавления статуса к заказу
        $order->statuses()->create([
            'status_option_id' => $this->settingsStore->getValue('default_status_option_id'),
            'changed_id' => $current_user_id,
            'comment' => '',
        ]);

        return redirect(route('orders.show', $order->id));
    }

    /**
     * Изменение существующего заказа.
     *
     * @param Request $request
     * @param Order $order
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function update(Request $request, Order $order)
    {
        $this->authorize('update', Order::class);

        $order->fill($request->all());
        $order->save();

        if ($order->getCurrentStatus()->option->id != $request->input('status_option_id')) {
            $order->statuses()->create([
                'status_option_id' => $request->input('status_option_id'),
                'changed_id' => Auth::user()->id,
                'comment' => $request->input('status_comment') ?? '',
            ]);
        }

        return redirect(route('orders.show', $order->id));
    }

    /**
     * Просмотр заказа.
     *
     * @param Order $order
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function show(Order $order)
    {
        $this->authorize('view', Order::class);

        $isFavorite = Favorite::where('user_id', Auth::user()->id)
            ->where('favoritable_type', Order::class)
            ->where('favoritable_id', $order->id)
            ->count();
        $isFavorite = $isFavorite > 0;

        return view('orders.show', compact('order', 'isFavorite'));
    }

    /**
     * Удаление заказа.
     *
     * @param Order $order
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function delete(Order $order, Request $request)
    {
        $this->authorize('delete', Order::class);

        $order->delete();

        $redirect_url = $request->query('redirect_uri');
        if ($redirect_url)
            return redirect($redirect_url);

        return redirect()->back();
    }

    /**
     * Добавляет заказ в избранное.
     *
     * @param Order $order
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function favorite(Order $order)
    {
        $this->authorize('update', Order::class);

        $existing_favorites_count = Favorite::where('user_id', Auth::user()->id)
            ->where('favoritable_id', $order->id)
            ->where('favoritable_type', Order::class)->count();

        if ($existing_favorites_count == 0)
            Favorite::create([
                'user_id' => Auth::user()->id,
                'favoritable_id' => $order->id,
                'favoritable_type' => Order::class,
            ]);

        return redirect(route('orders.show', $order->id));
    }

    /**
     * Удаляет заказ из избранного.
     *
     * @param Order $order
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    function unfavorite(Order $order)
    {
        Favorite::where([
            'user_id' => Auth::user()->id,
            'favoritable_id' => $order->id,
            'favoritable_type' => Order::class,
        ])->delete();

        return redirect(route('orders.show', $order->id));
    }
}
