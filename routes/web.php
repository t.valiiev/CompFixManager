<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

$this->post('logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['prefix' => 'users', 'except' => '/users/logout'], function () {
    // Authentication Routes...
    $this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
    $this->post('login', 'Auth\LoginController@login');

    // Registration Routes...
    $this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    $this->post('register', 'Auth\RegisterController@register');

    // Password Reset Routes...
    $this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    $this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    $this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    $this->post('password/reset', 'Auth\ResetPasswordController@reset');
});

Route::get('/login', ['as' => 'clients.loginForm', 'uses' => 'Auth\ClientLoginController@showLoginForm']);
Route::post('/login', ['as' => 'clients.login', 'uses' => 'Auth\ClientLoginController@login']);

Route::group(['middleware' => ['auth:clients']], function () {
    Route::get('/my_orders', ['as' => 'clients.orders', 'uses' => 'ClientController@client_orders']);
    Route::get('/my_orders/{order}', ['as' => 'clients.order', 'uses' => 'ClientController@client_order']);
});

Route::group(['prefix' => 'language'], function () {
    Route::get('{language?}', ['as' => 'language', 'uses' => 'LanguageController@changeLanguage']);
});

Route::group(['prefix' => 'themes'], function () {
    Route::get('{theme?}', ['as' => 'theme', 'uses' => 'ThemeController@changeTheme']);
});

Route::group(['middleware' => ['auth', 'block_ip', 'active_user'], 'prefix' => 'system'], function () {
    Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@index']);

    Route::get('/statistic', ['as' => 'statistic', 'uses' => 'StatisticController@index']);

    Route::group(['prefix' => 'users'], function () {
        Route::get('/', ['as' => 'users.index', 'uses' => 'UserController@index']);
        Route::get('/create', ['as' => 'users.create', 'uses' => 'UserController@create']);
        Route::get('/{user}/edit', ['as' => 'users.edit', 'uses' => 'UserController@edit']);
        Route::get('/{user}/delete', ['as' => 'users.delete', 'uses' => 'UserController@delete']);
        Route::put('/{user}', ['as' => 'users.update', 'uses' => 'UserController@update']);
        Route::post('/', ['as' => 'users.store', 'uses' => 'UserController@store']);
    });


    Route::group(['prefix' => 'groups'], function () {
        Route::get('/', ['as' => 'groups.index', 'uses' => 'GroupController@index']);
        Route::get('/create', ['as' => 'groups.create', 'uses' => 'GroupController@showCreateForm']);
        Route::get('/{group}/edit', ['as' => 'groups.edit', 'uses' => 'GroupController@showEditForm']);
        Route::get('/{group}/delete', ['as' => 'groups.delete', 'uses' => 'GroupController@delete']);
        Route::get('/{group}', ['as' => 'groups.show', 'uses' => 'GroupController@show']);
        Route::post('/', ['as' => 'groups.store', 'uses' => 'GroupController@store']);
        Route::put('/{group}', ['as' => 'groups.update', 'uses' => 'GroupController@update']);
    });


    Route::group(['prefix' => 'system_settings'], function () {
        Route::get('/', ['as' => 'system_settings', 'uses' => 'SystemSettingController@showUpdateForm']);
        Route::post('/', ['as' => 'system_settings.updateBasicSettings', 'uses' => 'SystemSettingController@updateBasicSettings']);
        Route::post('/graph', ['as' => 'system_settings.updateStatusGraph', 'uses' => 'SystemSettingController@updateStatusGraph']);
        Route::post('/status_option', ['as' => 'system_settings.addStatusOption', 'uses' => 'SystemSettingController@addStatusOption']);
        Route::post('/delete_status', ['as' => 'system_settings.deleteStatusOption', 'uses' => 'SystemSettingController@deleteStatusOption']);
        Route::post('/status_option/update', ['as' => 'system_settings.updateStatusOption', 'uses' => 'SystemSettingController@updateStatusOption']);
    });

    Route::group(['prefix' => 'orders'], function () {
        Route::get('/', ['as' => 'orders.index', 'uses' => 'OrderController@index']);
        Route::get('/create', ['as' => 'orders.create', 'uses' => 'OrderController@create']);
        Route::get('/create2', ['as' => 'orders.create2', 'uses' => 'OrderController@create2']);
        Route::get('/{order}', ['as' => 'orders.show', 'uses' => 'OrderController@show']);
        Route::get('/{order}/favorite', ['as' => 'orders.favorite', 'uses' => 'OrderController@favorite']);
        Route::get('/{order}/unfavorite', ['as' => 'orders.unfavorite', 'uses' => 'OrderController@unfavorite']);
        Route::get('/{order}/edit', ['as' => 'orders.edit', 'uses' => 'OrderController@edit']);
        Route::get('/{order}/delete', ['as' => 'orders.delete', 'uses' => 'OrderController@delete']);
        Route::post('/', ['as' => 'orders.store', 'uses' => 'OrderController@store']);
        Route::post('/store2', ['as' => 'orders.store2', 'uses' => 'OrderController@store2']);
        Route::put('/{order}', ['as' => 'orders.update', 'uses' => 'OrderController@update']);
    });

    Route::group(['prefix' => 'clients'], function () {
        Route::get('/', ['as' => 'clients.index', 'uses' => 'ClientController@index']);
        Route::get('/create', ['as' => 'clients.create', 'uses' => 'ClientController@create']);
        Route::get('/{client}', ['as' => 'clients.show', 'uses' => 'ClientController@show']);
        Route::get('/{client}/edit', ['as' => 'clients.edit', 'uses' => 'ClientController@edit']);
        Route::get('/{client}/delete', ['as' => 'clients.delete', 'uses' => 'ClientController@delete']);
        Route::post('/', ['as' => 'clients.store', 'uses' => 'ClientController@store']);
        Route::put('/{client}', ['as' => 'clients.update', 'uses' => 'ClientController@update']);
    });

    Route::group(['prefix' => 'devices'], function () {
        Route::get('/', ['as' => 'devices.index', 'uses' => 'DeviceController@index']);
        Route::get('/create', ['as' => 'devices.create', 'uses' => 'DeviceController@create']);
        Route::get('/{device}', ['as' => 'devices.show', 'uses' => 'DeviceController@show']);
        Route::get('/{device}/edit', ['as' => 'devices.edit', 'uses' => 'DeviceController@edit']);
        Route::get('/{device}/delete', ['as' => 'devices.delete', 'uses' => 'DeviceController@destroy']);
        Route::post('/', ['as' => 'devices.store', 'uses' => 'DeviceController@store']);
        Route::put('/{device}', ['as' => 'devices.update', 'uses' => 'DeviceController@update']);
    });
});
