@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                @component('orders.panel', [
                    'orders' => Auth::user()->favorite_orders,
                    'heading' => __('Your favorite orders'),
                    'showClient' => 1,
                    'showDevice' => 1,
                ])
                @endcomponent
            </div>
        </div>
    </div>
@endsection
