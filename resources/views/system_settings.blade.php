@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="btn btn-danger" onclick="window.history.back()">{{__('Back')}}</div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-6 col-md-offset-3">
            <form action="{{route('system_settings.updateBasicSettings')}}" method="post">
                {{csrf_field()}}
                {!! Form::make([
                    'default_user_group_id' => [
                        'type'=>'select', 'label'=>__('Default group for user').':', 'name'=>'default_user_group_id',
                        'value_field'=>'id','name_field'=>'name', 'empty_option'=>false,
                        'options'=>[
                            'values'=>$groups,
                            'selected'=>$settings['default_user_group_id'],
                        ]
                    ]
                ]) !!}
                <input type="submit" class="btn btn-success" value="{{__('Save')}}">
            </form>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h3>{{__('Add new option')}}:</h3>
            <form action="{{route('system_settings.addStatusOption')}}" method="post">
                {{csrf_field()}}
                {!! \App\Helpers\Form::input('text', 'name', __('New status name').':') !!}
                <input type="submit" class="btn btn-success" value="{{__('Add')}}">
            </form>
        </div>
    </div>
    <div class="row text-center">
        <h3>{{__('Status graph')}}:</h3>
        <form action="{{route('system_settings.updateStatusGraph')}}" method="post">
            {{csrf_field()}}
            <table class="table" style="width: auto; margin: 0 auto;">
                <thead>
                <tr>
                    <td>{{__('Default')}}</td>
                    <td></td>
                    @foreach($status_options as $status)
                        <td>{{__('To')}} "<span id="status-name-{{$status->id}}">{{$status->name}}</span>"</td>
                    @endforeach
                    <td>{{__('Actions')}}</td>
                </tr>
                </thead>
                <tbody>
                @foreach($status_options as $status_row)
                    <tr id="row-{{$status_row->id}}">

                        <td>
                            <input
                                    type="radio" name="default_status_option_id" class="default-status"
                                    value="{{$status_row->id}}"
                                    {{$settings['default_status_option_id'] == $status_row->id?'checked':''}}
                            >
                        </td>
                        <td class="edit-target">{{$status_row->name}}</td>
                        @foreach($status_options as $status_col)
                            @if($status_col->id == $status_row->id)
                                <td>X</td>
                            @else
                                <td>
                                    @php
                                        $title = "{$status_row->id}_{$status_col->id}";
                                        $checked = $status_graph[$title] ?? false
                                    @endphp
                                    <input name="{{$title}}" type="checkbox" {{$checked?'checked="checked"':''}}>
                                </td>
                            @endif
                        @endforeach
                        <td class="action-target">
                            <div class="primary">
                                <div class="btn btn-primary option-btn edit-option glyphicon glyphicon-pencil"
                                     data-id="{{$status_row->id}}">
                                </div>
                                <div class="btn btn-danger option-btn delete-option glyphicon glyphicon-remove"
                                     data-id="{{$status_row->id}}">
                                </div>
                            </div>
                            <div class="secondary" style="display: none;">
                                <div class="btn btn-success option-btn ok-option glyphicon glyphicon-ok m-r-l"
                                     data-id="{{$status_row->id}}">
                                </div>
                                <div class="btn btn-danger option-btn cancel-option glyphicon glyphicon-remove"
                                     data-id="{{$status_row->id}}">
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <input type="submit" class="btn btn-success" value="Save">
        </form>
    </div>
    <hr>
@stop

@push('scripts')
<script>
    var old_values = {};
    $.ajaxSetup({headers: {'Authorization': 'Bearer {{Auth::user()->api_token}}'}})

    $('.delete-option').click(function (e) {
        var option_id = e.target.getAttribute("data-id");
        console.log('Going to delete option with id =', option_id);
        $.post('{{route('system_settings.deleteStatusOption')}}', {id: option_id}, function (data) {
            window.location.reload();
        }).fail(function () {
            alert('You cannot delete this record, because you have something that points to it.');
        });
    });

    $('.edit-option').click(function (e) {
        var option_id = e.target.getAttribute('data-id');
        var $option_name_td = $('#row-' + option_id + ' .edit-target');

        $('#row-' + option_id + ' .action-target .primary').hide();
        $('#row-' + option_id + ' .action-target .secondary').show();

        old_values[option_id] = {text: $option_name_td.text()};

        $option_name_td.html('<input type="text" style="text-align: center" value="' + old_values[option_id].text + '"/>');
    });

    $('.cancel-option').click(function (e) {
        var option_id = e.target.getAttribute('data-id');
        var $option_name_td = $('#row-' + option_id + ' .edit-target');

        $('#row-' + option_id + ' .action-target .primary').show();
        $('#row-' + option_id + ' .action-target .secondary').hide();

        $option_name_td.html(old_values[option_id].text);
    });

    $('.ok-option').click(function (e) {
        var option_id = e.target.getAttribute('data-id');

        var value = $('#row-' + option_id + ' .edit-target input').val();

        $.post('{{route('system_settings.updateStatusOption')}}', {id: option_id, name: value}, function (data) {
            $('#status-name-' + option_id).html(value)
            $('#row-' + option_id + ' .edit-target').html(value);
            $('#row-' + option_id + ' .action-target .primary').show();
            $('#row-' + option_id + ' .action-target .secondary').hide();
        });
    });
</script>
@endpush