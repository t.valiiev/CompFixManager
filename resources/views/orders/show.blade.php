@extends('layouts.app')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title hidden-xs" style="display: inline; line-height: 36px;">{{$order->task}}</h2>
            @can('delete', \App\Order::class)
                <?php $url_d_o = route('orders.delete', $order->id) . "?redirect_uri=" . route('orders.index'); ?>
                <a href="{{$url_d_o}}" class="btn btn-danger pull-right hidden-xs">{{__('Delete')}}</a>
                <a href="{{$url_d_o}}" class="btn btn-danger pull-right visible-xs-block">
                    <i class="glyphicon glyphicon-remove"></i>
                </a>
            @endcan
            @can('update', \App\Order::class)
                <?php $url_u_o = route('orders.edit', $order->id); ?>
                <a href="{{$url_u_o}}" class="btn btn-primary pull-right m-r-1 hidden-xs">{{__('Edit')}}</a>
                <a href="{{$url_u_o}}" class="btn btn-primary pull-right m-r-1 visible-xs-block">
                    <i class="glyphicon glyphicon-edit"></i>
                </a>
            @endcan
            @if($isFavorite)
                <a href="{{route('orders.unfavorite', $order->id)}}"><i
                            class="glyphicon glyphicon-large glyphicon-star pull-right"></i></a>
            @else
                <a href="{{route('orders.favorite', $order->id)}}"><i
                            class="glyphicon glyphicon-large glyphicon-star-empty pull-right"></i></a>
            @endif
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="visible-xs-block" style="font-size: 22px;">{{$order->task}}</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    {{__('Comment')}}:
                    <p>{!! nl2br(htmlspecialchars($order->comment)) !!}</p>
                </div>
                <div class="col-md-6">
                    {{__('Device')}}:
                    <a href="{{route('devices.show', $order->device->id)}}">{{$order->device->hwid}}</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    {{__('Created by')}}:
                    <a href="{{route('users.edit', $order->creator)}}">{{$order->creator->name}}</a>
                </div>
                <div class="col-md-6">
                    {{__('Client')}}:
                    <a href="{{route('clients.show', $order->client->id)}}">{{$order->client->name}}</a>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">{{__('Status history')}}</div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>{{__('Status label')}}</th>
                    <th>{{__('Created at')}}</th>
                    <th>{{__('Created by')}}</th>
                    <th>{{__('Comment')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($order->statuses()->orderBy('created_at', 'desc')->get() as $status)
                    <tr>
                        <td>{{$status->option->name}} {{$loop->first ? '('.__('current').')':''}}</td>
                        <td>{{$status->created_at}} ({{$status->created_at->diffForHumans()}})</td>
                        <td>{{$status->user->name}}</td>
                        <td>{!! nl2br(htmlspecialchars($status->comment)) !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
