<div class="table-responsive">
    <table class="table">
        <thead>
        <tr>
            <th>{{__('Task')}}</th>
            @if($showDevice ?? false)
                <th>{{__('Device')}}</th>
            @endif
            @if($showClient ?? false)
                <th>{{__('Client')}}</th>
            @endif
            <th>{{__('Status')}}</th>
            <th class="text-center">{{__('Available actions')}}</th>
        </tr>
        </thead>
        <tbody>
        @forelse($orders as $order)
            <tr>
                <td><a href="{{route('orders.show', $order->id)}}">{{$order->task}}</a></td>
                @if($showDevice ?? false)
                    @if($order->device)
                        <td>
                            <a href="{{route('devices.show', $order->device->id)}}">
                                {{$order->device->type}} [{{$order->device->hwid}}]
                            </a>
                        </td>
                    @else
                        <td>---</td>
                    @endif
                @endif
                @if($showClient ?? false)
                    @if($order->client)
                        <td>
                            <a href="{{route('clients.show', $order->client->id)}}">
                                {{$order->client->name}}
                            </a>
                        </td>
                    @else
                        <td>---</td>
                    @endif
                @endif
                <td>{{$order->getCurrentStatusName()}}</td>
                <td class="text-center">
                    @can('update', \App\Order::class)
                        <a href="{{route("orders.edit", $order->id)}}" class="btn btn-primary">
                            <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i>
                        </a>
                    @endcan
                    @can('delete', \App\Order::class)
                        <a href="{{route("orders.delete", $order->id)}}" class="btn btn-danger">
                            <i class="glyphicon glyphicon-remove" aria-hidden="true"></i>
                        </a>
                    @endcan
                </td>
            </tr>
        @empty
            <tr>
                <td>{{__('No orders')}}</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        @endforelse
        </tbody>
    </table>
</div>