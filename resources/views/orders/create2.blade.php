@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <form id="advanced-form" action="{{route('orders.store2')}}" method="post" style="display: none;">
                {{csrf_field()}}
                <h3>{{__('Create or find client')}}</h3>
                <section>
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <label class="m-r-2">
                                <input type="radio" name="_new_client" id="existing-client" value="0"
                                       checked="checked" style="display: inline;">
                                <span style="font-size: 20px;">{{__('Find')}}</span>
                            </label>
                            <label>
                                <input type="radio" name="_new_client" id="new-client" value="1"
                                       style="display: inline;">
                                <span style="font-size: 20px;">{{__('Create')}}</span>
                            </label>
                        </div>
                    </div>
                    <div class="row" id="client-find-form">
                        <div class="col-xs-12">
                            {!! Form::make([
                                'client_id' => [
                                    'type' => 'select', 'label' => __('Find client by name, email or phone').':',
                                    'name_field' => 'name', 'value_field'=>'id',
                                    'empty_option' => true,
                                    'options' => [
                                        'values' => [],
                                        'selected' => old('client_id'),
                                    ],
                                ]
                            ]) !!}
                        </div>
                    </div>
                    <div class="row" id="client-create-form" style="display: none;">
                        <div class="col-xs-12">
                            {!! Form::make([
                                'client_name' => ['type'=> 'text', 'label'=>__('Name').':', 'value'=>old('client_name')],
                                'client_email' => ['type'=> 'text', 'label'=>__('Email').':', 'value'=>old('client_email')],
                                'client_phone' => ['type'=> 'text', 'label'=>__('Phone').':', 'value'=>old('client_phone')],
                                'client_comment' => ['type'=> 'textarea', 'rows' => 5, 'label'=>__('Comment').':', 'value'=>old('client_comment')],
                            ]) !!}
                        </div>
                    </div>
                </section>

                <h3>{{__('Create or find device')}}</h3>
                <section>
                    <div class="row">
                        <div class="col-xs-12 text-center">
                            <label class="m-r-2">
                                <input type="radio" name="_new_device" id="existing-device" value="0"
                                       style="display: inline;" disabled="disabled">
                                <span style="font-size: 20px;">{{__('Find')}}</span>
                            </label>
                            <label>
                                <input type="radio" name="_new_device" id="new-device" value="1"
                                       checked="checked" style="display: inline;">
                                <span style="font-size: 20px;">{{__('Create')}}</span>
                            </label>
                        </div>
                    </div>
                    <div class="row" id="device-find-form" style="display: none;">
                        <div class="col-xs-12">
                            {!! Form::make([
                                'device_id' => [
                                    'type' => 'select', 'label' => __('Select device').':',
                                    'name_field' => 'name', 'value_field'=>'id',
                                    'empty_option' => true,
                                    'options' => [
                                        'values' => [],
                                        'selected' => '',
                                    ],
                                ]
                            ]) !!}
                        </div>
                    </div>
                    <div class="row" id="device-create-form">
                        <div class="col-xs-12">
                            {!! Form::make([
                                'device_hwid' => [
                                    'type' => 'text', 'label' => __('HWID').':',
                                    'value'=>old('device_hwid')
                                ],
                                'device_type' => [
                                    'type' => 'text', 'label' => __('Type').':',
                                    'value'=>old('device_type')
                                ],
                                'device_description' => [
                                    'type' => 'textarea', 'label' => __('Description').':',
                                    'value'=>old('device_description'), 'rows' => 5
                                ],
                            ]) !!}
                        </div>
                    </div>
                </section>

                <h3>{{__('Create task')}}</h3>
                <section>
                    <div class="row">
                        <div class="col-xs-12">
                            {!! Form::make([
                                'order_task' => ['type'=> 'text', 'label'=>__('Task').':', 'value'=>old('order_task')],
                                'order_comment' => ['type'=> 'textarea', 'rows' => 7, 'label'=>__('Comment').':', 'value'=>old('order_comment')],
                            ]) !!}
                        </div>
                    </div>
                </section>
            </form>
        </div>
    </div>
@stop

@push('scripts')
<script>
    $.ajaxSetup({headers: {'Authorization': 'Bearer {{Auth::user()->api_token}}'}});

    var $form = $("#advanced-form");
    var form_options = {
        titleTemplate: '<span class="number">{{__('Step')}} #index#.</span> #title#',
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        labels: {
            next: '{{__("Next")}}',
            previous: '{{__("Previous")}}',
            finish: '{{__("Finish")}}',
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            console.log(event);
            return true;
        },
        onFinishing: function (event, currentIndex) {
            return true;
        },
        onFinished: function (event, currentIndex) {
            $form.submit();
        }
    };

    $form.steps(form_options);
    $form.fadeIn();



    var animation_time = 100;
    var delay = 20;

    var $device_find_form = $('#device-find-form');
    var $device_create_form = $('#device-create-form');
    var $new_device_option = $('#new-device');
    $new_device_option.click(function (e) {
        $device_find_form.fadeOut(animation_time)
        $device_create_form.delay(animation_time + delay).fadeIn(animation_time);
    });
    var $existing_device_option = $('#existing-device');
    $existing_device_option.click(function (e) {
        $device_create_form.fadeOut(animation_time);
        $device_find_form.delay(animation_time + delay).fadeIn(animation_time);
    });

    var $client_find_form = $('#client-find-form');
    var $client_create_form = $('#client-create-form');
    $('#new-client').click(function (e) {
        $client_find_form.fadeOut(animation_time);
        $client_create_form.delay(animation_time + delay).fadeIn(animation_time);

        $new_device_option.prop('checked', 'checked');
        $existing_device_option.prop('disabled', 1);
        $device_create_form.show();
        $device_find_form.hide();
    });
    $('#existing-client').click(function (e) {
        $client_create_form.fadeOut(animation_time);
        $client_find_form.delay(animation_time + delay).fadeIn(animation_time);

        $existing_device_option.prop('disabled', 0);
    });

    function formatClient(client) {
        if (client.loading) return '<div>{{__('Loading')}}...</div>';

        var search_condition = $client.data("select2").$dropdown.find("input").val();

        var name = client.name.split(search_condition);
        name = name[0] + (name[1] ? '<b><u>' + search_condition + '</u></b>' + name[1] : '');
        var phone = client.phone.split(search_condition);
        phone = phone[0] + (phone[1] ? '<b><u>' + search_condition + '</u></b>' + phone[1] : '');
        var email = client.email.split(search_condition);
        email = email[0] + (email[1] ? '<b><u>' + search_condition + '</u></b>' + email[1] : '');

        return '<div>' +
            '<span>' + name + '</span>' +
            '<p>{{__('Phone')}}: ' + phone + '<br>{{__('Email')}}: ' + email + '</p>' +
            '</div>';
    }

    function formatClientSelection(client) {
        return client.name || '';
    }

    var $client = $("#client_id");
    $client.select2({
        ajax: {
            url: '{{route('api.clients.list')}}',
            headers: {'Authorization': 'Bearer {{Auth::user()->api_token}}'},
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    "name:like": params.term,
                    "phone:like": params.term,
                    "email:like": params.term,
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;

                return {
                    results: data.data,
                    pagination: {
                        more: (params.page * data.per_page) < data.total
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        },
        minimumInputLength: 2,
        templateResult: formatClient,
        templateSelection: formatClientSelection
    })
        .on('select2:select', function updateDevices() {
            var client_id = $('#client_id').val();

            $.get('{{route('api.client.devices')}}', {client_id: client_id}, function (data) {
                $device.select2().empty();
                $device.select2({
                    disabled: false,
                    data: data,
                }, true);

                $device_find_form.show();
                $device_create_form.hide();
                $existing_device_option.prop('disabled', false)
                $existing_device_option.prop('checked', 'checked')
            });
        });

    var $device = $('#device_id');
    $device.select2({
        disabled: true,
        data: [],
    });
</script>
@endpush
