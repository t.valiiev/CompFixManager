@extends('layouts.app')

@section('content')
    @php
        unset($query['page']);
        $is_extended_search = count($query) >= 1;
    @endphp
    <div class="row p-b-1 search">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading mouse-pointer">
                    <h2 class="panel-title" style="display: inline; height: 100%;">
                        <i class="glyphicon glyphicon-search m1"></i>
                        {{__('Search')}}
                    </h2>
                </div>

                <div class="panel-body" {!! $is_extended_search?'':'style="display: none;"' !!}>
                    <form action="{{route('devices.index')}}" method="get">
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::input('text', 'hwid:like', __('HWID').':', $query['hwid:like']??'') !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::input('text', 'type:like', __('Type').':', $query['type:like']??'') !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::input('text', 'client_name', __('Client').':', $query['client_name']??'') !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <a class="btn btn-default" href="{{route('devices.index')}}">{{__('Clear form')}}</a>
                                <input type="submit" class="btn btn-primary" value="{{__('Search')}}">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @can('create', \App\Device::class)
        <div class="row">
            <div class="col-xs-12">
                <a href="{{route('devices.create')}}" class="btn btn-success">{{__('Create')}}</a>
            </div>
        </div>
    @endcan
    <div class="row">
        <div class="col-xs-12">
            {{$devices->links()}}
            @component('devices.table', ['devices' => $devices, 'showClient' => true])
            @endcomponent
            {{$devices->links()}}
        </div>
    </div>
@stop

@push('scripts')
<script>
    $.ajaxSetup({headers: {'Authorization': 'Bearer {{Auth::user()->api_token}}'}})

    $('input[name="client_name"]').autocomplete({
        source: "{{route('api.clients.names')}}",
        minLength: 2
    });

    $('input[name="hwid:like"]').autocomplete({
        source: "{{route('api.devices.hwids')}}",
        minLength: 2
    });
</script>
@endpush

@push('scripts')
<script>
    var extended_search = '{{$is_extended_search}}' == '1';
    $('.search .panel-heading').click(function () {
        extended_search = !extended_search;

        var $search_body = $('.search .panel-body');
        if (extended_search) {
            $search_body.show('blind');
        } else {
            $search_body.hide('blind');
        }
    });
</script>
@endpush
