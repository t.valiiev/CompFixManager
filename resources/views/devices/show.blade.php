@extends('layouts.app')
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            @can('delete', \App\Device::class)
                <?php $url_d_d = route('devices.delete', $device->id) . "?redirect_uri=" . route('devices.index'); ?>
                <a href="{{$url_d_d}}" class="btn btn-danger pull-right hidden-xs">{{__('Delete')}}</a>
                <a href="{{$url_d_d}}" class="btn btn-danger pull-right visible-xs-block">
                    <i class="glyphicon glyphicon-remove"></i>
                </a>
            @endcan
            @can('update', \App\Device::class)
                <?php $url_e_d = route('devices.edit', $device->id); ?>
                <a href="{{$url_e_d}}" class="btn btn-primary pull-right m-r-1 hidden-xs">{{__('Edit')}}</a>
                <a href="{{$url_e_d}}" class="btn btn-primary pull-right m-r-1 visible-xs-block">
                    <i class="glyphicon glyphicon-edit"></i>
                </a>
            @endcan
            @can('create', \App\Device::class)
                <?php $url_c_d = route('orders.create') . "?device_id=" . $device->id; ?>
                <a class="btn btn-success pull-right m-r-1 hidden-xs" href="{{$url_c_d}}">
                    {{__('Create order with this device')}}
                </a>
                <a class="btn btn-success pull-right m-r-1 visible-xs-block" href="{{$url_c_d}}">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>{{__('Order')}}</span>
                </a>
            @endcan
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    {{__('HWID')}}: {{$device->hwid}}
                </div>
                <div class="col-md-6">{{__('Type')}}: {{$device->type}}</div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    {{__('Description')}}:
                    <p>{!! nl2br(htmlspecialchars($device->description)) !!}</p>
                </div>
                <div class="col-md-6">
                    {{__('Owner')}}:
                    <a href="{{route('clients.show', $device->client->id)}}">{{$device->client->name}}</a>
                </div>
            </div>
        </div>
    </div>

    @if($device->orders)
        @component('orders.panel', ['orders' => $device->orders, 'heading' => __('Related orders'), 'showClient' => 1])
        @endcomponent
    @endif
@stop
