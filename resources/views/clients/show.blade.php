@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title hidden-xs" style="display: inline; line-height: 36px;">{{$client->name}}</h2>
                    @can('delete', \App\Client::class)
                        <?php $url_d_c = route('clients.delete', $client->id) . "?redirect_uri=" . route('clients.index'); ?>
                        <a href="{{$url_d_c}}" class="btn btn-danger pull-right hidden-xs">{{__('Delete')}}</a>
                        <a href="{{$url_d_c}}" class="btn btn-danger pull-right visible-xs-block">
                            <i class="glyphicon glyphicon-remove"></i>
                        </a>
                    @endcan
                    @can('update', \App\Client::class)
                        <?php $url_e_c = route('clients.edit', $client->id); ?>
                        <a href="{{$url_e_c}}" class="btn btn-primary pull-right m-r-1 hidden-xs">{{__('Edit')}}</a>
                        <a href="{{$url_e_c}}" class="btn btn-primary pull-right m-r-1 visible-xs-block">
                            <i class="glyphicon glyphicon-edit"></i>
                        </a>
                    @endcan
                    @can('create', \App\Device::class)
                        <?php $url_c_d = route('devices.create') . "?client_id=" . $client->id; ?>
                        <a href="{{$url_c_d}}" class="btn btn-success pull-right m-r-1 hidden-xs">
                            {{__('Create device')}}
                        </a>
                        <a href="{{$url_c_d}}" class="btn btn-success pull-right m-r-1 visible-xs-block">
                            <i class="glyphicon glyphicon-plus"></i>
                            <span>{{__('Device')}}</span>
                        </a>
                    @endcan
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <h1 class="visible-xs-block" style="font-size: 22px;">{{$client->name}}</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            {{__('Email')}}:
                            <p>{!! nl2br(htmlspecialchars($client->email)) !!}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            {{__('Phone')}}:
                            <p>{!! nl2br(htmlspecialchars($client->phone)) !!}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            {{__('Comment')}}:
                            <p>{!! nl2br(htmlspecialchars($client->comment)) !!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if($client->devices)
        @component('devices.panel', ['devices' => $client->devices, 'heading' => __('Client`s devices').':'])
        @endcomponent
    @endif

    @if($client->orders)
        @component('orders.panel', ['orders' => $client->orders, 'heading' => __('Client`s orders').':', 'showDevice' => 1])
        @endcomponent
    @endif
@stop
